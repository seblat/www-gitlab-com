---
layout: markdown_page
title: "About GitLab Community Edition"
---

If you're interested in using GitLab, we recommend you [download and install
GitLab Enterprise Edition][ee], even if you're not sure whether you will ever
subscribe for a license for [GitLab Enterprise Edition][ee]. You will still be able
to make use of all features of GitLab Community Edition without a need for a license.

## License model

GitLab is build on a an open core model. That means there are two versions of GitLab:
Community Edition and Enterprise Edition.

GitLab Community Edition is open source, with a MIT Expat license. GitLab Enterprise Edition
is built on top of Community Edition: it uses the same _core_, but adds additional features
and functionality on top of that. This is under a proprietary license.

For both versions: All javascript code in GitLab is open source. All javascript code written by
GitLab is under the same MIT license.

## Why use Enterprise Edition

To be able to use GitLab Enterprise Edition features, you need a subscription which will give you a license.
If you're using Enterprise Edition without a license, you are only using functions that are MIT licensed.

This means that if you have installed GitLab Enterprise Edition without a license, you will not notice a
difference with a typical Community Edition instance, but you have additional advantages:

1. If at any point in time you'd like to trial Enterprise Edition features, you can do this without the
need of setting up a new instance or upgrading your existing instance.
You simply enable the trial from within GitLab. If you're not satisfied
with the Enterprise Edition features, your instance will automatically revert back to only Community Edition
features after the trial has expired.
1. To upgrade from Community Edition to Enterprise Edition you will have
to make sure you're on the same version and follow specific steps, which often require downtime.
Using Enterprise Edition, changing between only Community Edition features and the full suite of Enterprise Edition features
is a matter of a single click.

## Community Edition

We recommend you [install GitLab Enterprise Edition][ee]. Alternatively, you can [install Community Edition][ce].

[ce]: /installation/?version=ce
[ee]: /installation