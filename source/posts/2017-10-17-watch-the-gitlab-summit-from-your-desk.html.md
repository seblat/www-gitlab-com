---
title: "We're coming to you live from Crete, at the GitLab Summit!"
author: Emily von Hoffmann
author_twitter: emvonhoffmann
author_gitlab: evhoffmann
categories: GitLab
image_title: '/images/blogimages/greece-summit-2017.png'
description: 'Read on for all the events you can watch and participate in.'
cta_button_text: 'Watch live'
cta_button_link: 'https://www.youtube.com/watch?v=t00MUfDfQoM'
---

It's that time again! Every nine months, our entire remote workforce descends on one location for the [GitLab Summit](https://about.gitlab.com/culture/summits/). This year, we'll be in Crete, and you're invited! 

<!-- more -->

Before you go off and buy a plane ticket, we should clarify that there probably isn't room for all of you on the island. But we're trying something new this Summit — we're live streaming every day from 9 to 9, to bring the experience to as many of our remote friends as possible. 

## Watch

We'll be streaming on [YouTube](https://www.youtube.com/watch?v=t00MUfDfQoM) and [Facebook](https://www.facebook.com/gitlab/), so you can watch however you prefer. [Sid](https://about.gitlab.com/team/#sytses) and [Dmitriy](https://about.gitlab.com/team/#dzaporozhets)'s keynotes, our Santorini trip, GitLabber-led user generated content (UGC) sessions, the 10.1 release, and our final party can all be viewed in one place. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/t00MUfDfQoM" frameborder="0" allowfullscreen></iframe>

## Schedule

We'll likely make some changes to this schedule as we close in on kickoff, so please keep in mind it's a WIP! 

#### Thursday, October 19
* 8am-6pm UTC - Arrivals and getting to know GitLabbers 

#### Friday, October 20
* 6-7am UTC - AMAs - Send us your questions ahead of time on Twitter with #GitLabSummit
  * 6-6:15am  Mark Pundsack ([@MarkPundsack](https://twitter.com/MarkPundsack)), Head of Product 
  * 6:15-6:30am Barbie Graver ([@BarbieGraver](https://twitter.com/barbiegraver)), Chief Culture Officer
  * 6:30-6:45am Sarrah Vesselov ([@SVesselov](https://twitter.com/svesselov)), UX Lead
* 7am UTC - Welcome & keynote with GitLab CEO Sid Sijbrandij ([@sytses](https://twitter.com/sytses))
* 8am UTC - AMA with GitLab CEO Sid Sijbrandij (chat your questions here or on Twitter using #GitLabSummit)
* 9am UTC - Eat lunch with us! 
* 10am UTC - Join us live for our Amazing Race challenge
* 12pm UTC - How GitLab Started keynote with CTO & Co-founder Dmitriy Zaporozhets ([@dzaporozhets](https://twitter.com/dzaporozhets);chat your questions on YouTube or on Twitter using #GitLabSummit)
* 1:15pm UTC - Award Ceremony and Happy Hour
* 3pm UTC - GitLab BBQ 

#### Saturday, October 21
* 4am - 5pm UTC Day trip to Santorini 
* 5:15 pm UTC - Join us for dinner and hallway conversations 

#### Sunday, October 22
* 6am UTC - Join us for breakfast
* 7am UTC - Chat with our developers and engineers as they release GitLab 10.1 
* 9am-3pm UTC - Day trip to Heraklion

#### Monday, October 23
* 6-11am UTC - Send us your questions to have them answered live
* 11am UTC - UGC Session 1 
* 12pm UTC - UGC Session 2
* 1pm UTC - UGC Session 3
* 2pm UTC - UGC Session 4
* 3pm UTC - Join us for dinner
* 5pm UTC - Join us for Game Night and a [Gitter AMA](https://gitter.im/gitterHQ/topics/categories/ama)

#### Tuesday, October 24
* 6-11am UTC Send us your questions to have them answered live
* 11am UTC - UGC Session 1 
* 12pm UTC - UGC Session 2
* 1pm UTC - UGC Session 3
* 2pm UTC - UGC Session 4
* 3pm UTC - Join us for dinner
* 5pm UTC - Join the Toga Party!

## Get involved

We want to see you there! [Tweet us](https://twitter.com/gitlab) using #GitLabSummit to let us know your questions and comments. We'll be giving away limited edition swag to people who chime in and ask questions on social, and we'll also poll you to ask which UGC sessions you want live streamed. We're so excited to share the Summit with our community for the first time, and we hope you'll join us! 

Read more about our company values in our [open source](https://about.gitlab.com/2016/07/12/our-handbook-is-open-source-heres-why/) [handbook](https://about.gitlab.com/handbook/values), licensed by [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
